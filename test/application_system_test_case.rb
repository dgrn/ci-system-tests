require "test_helper"
require "webdrivers" unless ENV["SELENIUM_REMOTE_URL"]

Capybara.register_driver :chrome_remote do |app|
  capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
    chromeOptions: { args: %w[headless disable-gpu] }
  )

  Capybara::Selenium::Driver.new app,
                                 url: ENV["SELENIUM_REMOTE_URL"],
                                 browser: :remote,
                                 desired_capabilities: capabilities
end

# Bind the server to an externally accessible IP. This is especially important
# when used in Docker.
Capybara.server_port = 8200
Capybara.server_host = Socket.ip_address_list.detect(&:ipv4_private?).ip_address

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  if ENV["SELENIUM_REMOTE_URL"]
    driven_by :chrome_remote
  else
    driven_by :selenium, using: :headless_chrome, screen_size: [1400, 1400]
  end

  def setup
    host! "http://#{Capybara.server_host}:#{Capybara.server_port}"

    super
  end
end
