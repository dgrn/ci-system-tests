require "application_system_test_case"

class RootTest < ApplicationSystemTestCase
   test "visiting the index" do
     visit root_path

     assert_selector "h1", text: "Yay!"
   end
end
