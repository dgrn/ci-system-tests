FROM ruby:2.6.3
RUN apt-get update && apt-get install -y build-essential nodejs && rm -rf /var/lib/apt/lists/*

ENV APP_HOME /gitlab-ci-test
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

COPY Gemfile* $APP_HOME/
RUN gem install bundler --no-doc && bundle install

COPY . $APP_HOME
